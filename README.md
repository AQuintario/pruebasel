Okuant Data Analysis Department - Access test

In this test you are given two datasets: vec ('vecinos') and nuc ('nucleos').
Both correspond to real estate ads. Your goal will be to provide an assesment
of nucleos's prices based on their nearest vecinos.
We have provided on both tables coordinate projections x and y (in km) for your
convinience.

You will establish the comparability between each nuc and its vecs based not only
on location, but also attending to some of the intrinsic characteristics of the
dwellings. You will be evaluated by what charactesitics you choose and how you
use them (e.g. how to compare surface).

After selecting vecs for each nuc, you will extract statistics on price and/or
price/m2. The goal here is both to get an estimation of the nuc price and also
some information about the quality of every evaluation.

You will use the data.table package for processing the data.
The report can be done in markdown, html, jupyter notebook or just a very well
formatted and commented R script.
Non-reproducible code or code that doesn't run as a standalone will be rejected.
Document every step of your process, explain and justify all your choices and
show your work in a clear way.
